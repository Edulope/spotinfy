class CreateMusicBelongsToCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :music_belongs_to_categories do |t|
      t.references :music, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
