class CreatePlaylistContainsMusics < ActiveRecord::Migration[5.2]
  def change
    create_table :playlist_contains_musics do |t|
      t.references :music, foreign_key: true
      t.references :playlist, foreign_key: true

      t.timestamps
    end
  end
end
