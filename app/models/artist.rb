class Artist < User
    has_and_belongs_to_many :users;
    has_many :albums;
    has_and_belongs_to_many :musics;
end
