class Music < ApplicationRecord
    has_and_belongs_to_many :users;
    belongs_to :album;
    has_and_belongs_to_many :categories;
    has_and_belongs_to_many :playlists;
    has_and_belongs_to_many :artists;
end
