class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  has_and_belongs_to_many :musics;
  has_and_belongs_to_many :playlists;
  has_and_belongs_to_many :categories;
  has_and_belongs_to_many :albums;
  has_and_belongs_to_many :artists;
end
